// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

interface IERC721{
    function transferFrom(address _from, address _to, uint _id) external;
}


contract Escrow{

    address public nftAddress;
    uint public nftID;
    uint public pruchasePrice;
    uint public escrowAmount;
    address payable seller;
    address payable buyer;
    address public inspector;
    address public lender;
    bool public inspectionPassed = false;
    mapping(address => bool) approvals;

    constructor(address _nftAddress, 
        uint _nftID, 
        uint _purchasePrice,
        uint _escrowAmount,
        address payable _seller, 
        address payable _buyer,
        address _inspector,
        address _lender){
        nftAddress = _nftAddress;
        nftID = _nftID;
        seller = _seller;
        buyer = _buyer;
        pruchasePrice = _purchasePrice;
        escrowAmount = _escrowAmount;
        inspector = _inspector;
        lender = _lender;
    }

    modifier onlyBuyer(){
        require(msg.sender == buyer,'Earnest depositer should be buyer');
        _;
    }

    modifier onlyInspector(){
        require(msg.sender == inspector,'Only inspector can approve inspection');
        _;
    }

    modifier onlyLender(){
        require(msg.sender == lender,'Only lender can pay');
        _;
    }

    receive() external payable{

    }

    function depositEarnest() public payable onlyBuyer {
        require(msg.value >= escrowAmount,'Not enough escrow amount');
    }

    function payLendingAmount() public payable onlyLender {
        require(msg.value >= pruchasePrice - escrowAmount);
    }

    function updateInspectionStatus(bool _passed) public onlyInspector {
        inspectionPassed = _passed;
    }

    function approve() public {
        approvals[msg.sender]=true;
    }

    function getInspectionStatus() public view returns (bool){
        return inspectionPassed;
    }

    function getBalance() public view returns( uint ){
        return address(this).balance;
    }



    function finalizeSale() public {
        require(inspectionPassed,'Must pass inspection');
        require(approvals[buyer],'must be approved by buyer');
        require(approvals[seller],'must be approved by seller');
        require(approvals[lender],'must be approved by lender');
        require(address(this).balance >= pruchasePrice,'Complete payment not received');

        (bool success, ) = payable(seller).call{value : address(this).balance}('');
        require(success,'Payment transfer failed');

        IERC721(nftAddress).transferFrom(seller, buyer, nftID);
    }
}

