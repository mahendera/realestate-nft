const { expect } = require('chai');
const { ethers } = require('hardhat');

describe('Counter', () => {
    let counter;

    beforeEach(async () => {
        const Counter = await ethers.getContractFactory('Counter')
        counter = await Counter.deploy('My Counter', 1)
    });

    describe('Deployment', () => {
        it('sets the initial count', async () => {
            const count = await counter.count()
            expect(count).to.equal(1)
        })

        it('sets the initial name', async () => {
            const name = await counter.name()
            expect(name).to.equal('My Counter')
        })
    });

    describe('Counting', ()=>{

        it('Increments the count', async ()=>{
            await counter.increment();
            expect(await counter.count()).to.equal(2)

            await counter.increment();
            expect(await counter.count()).to.equal(3)
        })

        it('Decrements the count', async ()=>{
            let transaction = await counter.decrement();
            await transaction.wait();
            expect(await counter.count()).to.equal(0)
            await expect(counter.decrement()).to.be.reverted

        })

        it('Reads name', async () =>{
            expect(await counter.name()).to.equal('My Counter')
        })

        it('Updates name', async () =>{
            await counter.setName("Mahender")
            expect(await counter.name()).to.equal('Mahender')
        })

    })

});