const { expect } = require('chai')
const { ethers } = require('hardhat')

const tokens = (n) => {
    return ethers.utils.parseUnits(n.toString(), 'ether');
}

const ether = tokens;

describe('RealEstate', () => {

    let realEstate;
    let escrow;
    let deployer;
    let seller, buyer;
    let nftID = 1;
    let purchasePrices = ether(100)
    let escrowAmount = ether(20)

    beforeEach(async () => {
        accounts = await ethers.getSigners();
        seller = accounts[0]
        deployer = seller
        buyer = accounts[1]
        inspector = accounts[2];
        lender = accounts[3];


        const RealEstate = await ethers.getContractFactory('RealEstate')
        const Escrow = await ethers.getContractFactory('Escrow')

        realEstate = await RealEstate.deploy();
        escrow = await Escrow.deploy(realEstate.address,
            nftID,
            ether(100),
            ether(20),
            seller.address,
            buyer.address,
            inspector.address,
            lender.address);

        // seller approves nft
        let traction = await realEstate.connect(seller).approve(escrow.address, nftID);
        await traction.wait();
    })

    describe('Deployment', () => {

        it('send an NFT to the seller/deployer', async () => {
            expect(await realEstate.ownerOf(nftID)).to.equal(seller.address)
        })
    })

    describe('Selling real estate', () => {

        it('successfully executing sale', async () => {
            let transaction, balance;
            expect(await realEstate.ownerOf(nftID)).to.equal(seller.address)

            transaction = await escrow.connect(buyer).depositEarnest({ value: ether(20) });
            console.log("Buyer paid earnest amount")

            balance = await escrow.getBalance();
            console.log("escrow balance ", ethers.utils.formatEther(balance));

            transaction = await escrow.connect(inspector).updateInspectionStatus(true);
            await transaction.wait();
            console.log("inspection status : ", await escrow.inspectionPassed());

            // buyer approval
            transaction = await escrow.connect(buyer).approve();
            await transaction.wait();

            // seller approval
            transaction = await escrow.connect(seller).approve();
            await transaction.wait();

            // pay lending amount
            transaction = await lender.sendTransaction({to : escrow.address, value : ether(80)})
            await transaction.wait();


            // lender approval
            transaction = await escrow.connect(lender).approve();
            await transaction.wait();

            // check balance

            console.log("balance in escrow :", ethers.utils.formatEther(await escrow.getBalance()));

            transaction = await escrow.connect(buyer).finalizeSale();
            await transaction.wait();
            console.log("Buyer finalizes sale");

            let sellerBalance = await ethers.provider.getBalance(seller.address);
            console.log("seller balance ", sellerBalance);
            expect(sellerBalance).to.be.above(10099);
            expect(await realEstate.ownerOf(nftID)).to.equal(buyer.address)
        })
    })

})